#!/usr/bin/env bash

CFGFILE=test/test.js

usage() { # ojimbo
  printf 'USAGE: %s <set|reset>\n' $0
  printf '\tset:\tset config values to use proxy\n'
  printf '\treset:\tset config values to default\n'
}

if [[ -e "${CFGFILE}" ]]; then
  cp "${CFGFILE}" "${CFGFILE}"_bak_$(date -I)-$(date "+%H%M%S")
else
  echo ERROR: config file to edit not found
  exit 15
fi
if [[ $( ps -ef | grep -c [s]ailfish-brow ) -ne 0 ]]; then
  echo ERROR: Browser is running, please close it first!
  exit 25
fi

set_config() {
  key=$1
  val=$2
  if [[ "x$val" = "x" ]]; then
	val=\"\"
  fi
  #user_pref("network.proxy.http", "127.0.0.1");
  #user_pref("network.proxy.http_port", 8118);
  echo sed -e "s@\(^user_pref(\"${key}\"\) \(.*$\)@\"\\1${val}\");@" ${CFGFILE}
  sed -e "s@\(^user_pref(\"${key}\"\) \(.*$\)@\"\\1${val}\");@" ${CFGFILE}
}

case $1 in
  set)
	set_config "network.proxy.http" "127.0.0.1"
	set_config "network.proxy.http_port" "8118"
	set_config "network.proxy.ssl" "127.0.0.1"
	set_config "network.proxy.ssl_port" "8118"
	set_config "network.proxy.no_proxies_on" "localhost, 127.0.0.1, 192.168.0.0"
	set_config "network.proxy.proxy_over_tls" "false"
	set_config "network.proxy.type" "1"
	;;
  reset)
	set_config "network.proxy.http" ""
	set_config "network.proxy.http_port" ""
	set_config "network.proxy.ssl" ""
	set_config "network.proxy.ssl_port" ""
	set_config "network.proxy.no_proxies_on" ""
	set_config "network.proxy.proxy_over_tls" "false"
	set_config "network.proxy.type" "0"
	;;
  *) usage;;

esac
