#!/usr/bin/env python3

import http.server
#import socketserver
import os, sys, logging
from functools import lru_cache
import re

loglevel=os.environ.get("LOGLEVEL", "INFO")
logging.basicConfig(stream=sys.stderr, level=loglevel)


# TODO: add journal logging:
# from systemd.journal import JournaldLogHandler
# # get an instance of the logger object this module will use
# logger = logging.getLogger(__name__)
# # instantiate the JournaldLogHandler to hook into systemd
# journald_handler = JournaldLogHandler()
# # set a formatter to include the level name
# journald_handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
# # add the journald handler to the current logger
# logger.addHandler(journald_handler)

webroot=os.environ.get("WEBROOT", ".")
webaddr=os.environ.get("WEBADDR", "127.0.0.1")
webport=os.environ.get("WEBPORT", "8119")

@lru_cache
def rewrite_req(req):
    # rewrite request to filesystem path:
    # first match: /www.example.org/ab2p.css --> /org/example/www/ab2p.css
    p = re.compile(r"^/([^/]*?)\.([^/.]+)(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?/ab2p.css$")
    serverpath = p.sub( r"/\9/\8/\7/\6/\5/\4/\3/\2/\1/ab2p.css", req)
    if not serverpath:
      logging.warning ('rewrite pattern 1 did not match.')
      return None
    # second match: /www.example.org/ab2p.css --> /org/example/ab2p.css
    p = re.compile(r"(^.*/+)[^/]+/+ab2p.css$")
    domainpath = p.sub(r"\1ab2p.css", serverpath)
    if not domainpath:
      logging.warning ('rewrite pattern 2 did not match.')
      return None

    # pointless cleanup, get rid of the leading slashes:
    p = re.compile(r"^/+(/.*)")
    serverpath = p.sub(r"\1", serverpath)
    domainpath = p.sub(r"\1", domainpath)

    logging.debug("looking for %s or %s in webdir %s", serverpath, domainpath, webdir)
    if os.path.isfile(webdir + serverpath):
      return webdir + serverpath
    elif os.path.isfile(webdir + domainpath):
      return webdir + domainpath
    else:
      return None


class Handler(http.server.SimpleHTTPRequestHandler):
    directory = webroot
    global webdir
    webdir = directory + "/css"
    logging.info ('Initializing request handler serving files from \'%s\', and css files under %s', webroot, webdir)

    @lru_cache
    def do_HEAD(self):
        return Handler.do_GET(self, "true")

    @lru_cache
    def do_GET(self):
        return Handler.do_GET(self, "false")

    @lru_cache
    def do_GET(self, head = "false"):

        ct_image = "false"
        reqpath = self.path
        logging.debug('got request: \'%s\'', reqpath)
        if head == "true":
          logging.debug('operating in HEAD mode')

        # serve /*.css
        if re.match(r"^/(|debug/)ab2p(|\.common)\.css$", reqpath):
          logging.debug('special request for root css detected.')
          if not os.path.isfile(webdir + reqpath):
            self.path = None
          else:
            self.path = webdir + reqpath
         # serve "blocked" images as well
        elif re.match(r"^/blocked.*\.(jp?g|png)$", reqpath):
          logging.debug('special request for image detected.')
          if not os.path.isfile(webroot + reqpath):
            self.path = None
          else:
            self.path = webroot + reqpath
            ct_image = "true"
        else:
          # do the request - > filepath rewrite spiel
          self.path = rewrite_req(self.path)

        if not self.path:
          logging.debug('something went wrong. doing 404.')
          self.send_error(404, "not found",  "not found")
          return

        try:
          # Respond with the file.
          logging.debug('serving %s, head mode is %s', self.path, head)
          self.send_response(200)
          self.send_header("Cache-Control", "public,max-age=604800,immutable")
          if ct_image == "true":
            logging.debug('we are sending an image...')
            if self.path.endswith("jpg") or self.path.endswith("jpeg"):
              self.send_header("Content-Type", "image/jpeg;charset=utf-8")
            elif self.path.endswith("png"):
              self.send_header("Content-Type", "image/png;charset=utf-8")
            elif self.path.endswith("gif"):
              self.send_header("Content-Type", "image/gif;charset=utf-8")
            else:
              self.send_header("Content-Type", "application/octet-stream;charset=utf-8")
          else:
            self.send_header("Content-Type", "text/css;charset=utf-8")
          self.send_header("Content-Length", os.stat(self.path).st_size)
          self.end_headers()
          if head == "false":
            #logging.debug('reading file...')
            #content = open(self.path, 'rb').read()
            #logging.debug('sending %d bytes',  os.stat(self.path).st_size)
            #self.wfile.write(content)
            with open(self.path, 'rb') as fdata:
              self.wfile.write(fdata.read())
        except:
          self.send_error(500, "server failure", "server failure")



## int main() ;)
server_address = (webaddr, int(webport))

logging.info ('Starting Server on %s:%s...', webaddr, webport)
logging.info ('Loglevel set to %s', loglevel)
#with socketserver.TCPServer(server_address, Handler) as httpd:
with http.server.ThreadingHTTPServer(server_address, Handler) as httpd:
  try:
    logging.info ('Server started.')
    httpd.serve_forever()
    logging.info ('Server stopped.')
  except (SystemExit, KeyboardInterrupt):
    logging.warning(  "Server exiting...")
    if loglevel == "DEBUG":
        logging.debug("LRU cache stats:")
        logging.debug(rewrite_req.cache_info())
        logging.debug(Handler.do_GET.cache_info())
        logging.debug(Handler.do_HEAD.cache_info())
    httpd.shutdown()
    pass

# TODO: catch SIGHUP and re-exec ourselves:
    #  os.execl(sys.executable, os.path.abspath(__file__), *sys.argv) 
