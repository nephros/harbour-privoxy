#!/bin/sh
#openssl req -new -x509 -extensions v3_ca -passout pass:"ahoisailors" -keyout privoxy-ca-key.pem -out privoxy-ca-cert.crt -days 3650 -config privoxy-ca-csr.cnf
openssl req -new -x509 -days 3650  -passout pass:"ahoisailors" -config harbour-privoxy-ca.cnf -keyout privoxy-ca-key.pem -out privoxy-ca-cert.crt
chown root:inet privoxy-ca-key.pem privoxy-ca-cert.crt
chmod 640 privoxy-ca-key.pem privoxy-ca-cert.crt

# The below generate EC certificates. For future use.

# Be sure to match the curve/key with the sourcecode in privoxy/wolfssl.c
# we use the smalleats supported curve, secp224r1
# security really isn't an issue between local privoxy and local browser
# also, 224 bits is equivalent to 2048 bbit RSA anyway.
# See 'openssl ecparam -list_curves'
# NB: The curve name is the only parameter to the ec key type; it defines both the curve characteristics and the key size.
# openssl req -new -newkey ec -pkeyopt ec_paramgen_curve:secp224r1 -sha224 -x509 -days 3650  -passout pass:"ahoisailors" -config harbour-privoxy-ca.cnf -keyout privoxy-ca-ecc-key.pem -out privoxy-ca-ecc-cert.crt
# chown root:inet privoxy-ca-ecc-key.pem privoxy-ca-ecc-cert.crt
# chmod 640 privoxy-ca-key.pem privoxy-ca-cert.crt

# openssl req -new -newkey ed25519 -x509 -days 3650 -passout pass:"ahoisailors" -config harbour-privoxy-ca.cnf -keyout privoxy-ca-ed25519-key.pem -out privoxy-ca-ed25519-cert.crt
# chown root:inet privoxy-ca-ed25519-key.pem privoxy-ca-ed25519-cert.crt
# chmod 640 privoxy-ca-ed25519-key.pem privoxy-ca-ed25519-cert.crt

printf "\nOkay, now you need to install the CA onto the system.\nPlease see the README in the harbour-privoxy source repo on how to.\n"
