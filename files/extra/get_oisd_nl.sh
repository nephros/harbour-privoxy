###
### simple script to convert blocking hosts files to privoxy filters
###

function usage() {
	printf "USAGE: %s small|big|nsfw|nsfw-small\n\nSee https://oisd.nl/setup for details\n\n" "$0"
}
if [ -z "$1" ]; then
	usage
	exit 1
fi
which=$1
case $which in
	small) ;;
	big) ;;
	nsfw) ;;
	nsfw-small) ;;
	*) printf "unknown list: %s\n" "$which"; usage; exit 1 ;;
esac
out="${which}_oisd_nl.action"
printf "{+block{Host matches pattern from %s.oisd.nl}}\n" "${which}" > ${out}
tmp=$(mktemp)
printf "Downloading list...\n"
# this used to be there, not any more...
#curl -s -L -o "${tmp}" "https://${which}.oisd.nl/regex"
curl -s -L -o "${tmp}" "https://${which}.oisd.nl/domainswild"
grep -v ^# "${tmp}" | grep -v ^$ >> ${out}
printf "Created/updated action file at %s.\nRemember to add it to your Privoxy config file.\n" "${out}" 
