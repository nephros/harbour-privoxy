#!/bin/bash

###
### simple script to convert blocking hosts files to privoxy filters
###

tmpdir=$(mktemp -d)

action=hosts_consolidated.action

danpollock=${tmpdir}/danpollock_hosts.txt
mvps=${tmpdir}/mvps_hosts.txt
peterlowe=${tmpdir}/peterlowe_hosts.txt
echo "downloading hosts files..."
curl -s -L -o $mvps "https://winhelp2002.mvps.org/hosts.txt" || exit 1
curl -s -L -o $danpollock "https://someonewhocares.org/hosts/hosts" || exit 1
curl -s -L -o $peterlowe "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=1&mimetype=plaintext" || exit 1
lists="$mvps $danpollock $peterlowe"

for inf in $lists; do
    [ -r $inf ] || exit 1
    printf 'converting %u lines in %s\n'  $(wc -l ${inf} | sed 's@/.*/@@')
    tmp=$(mktemp)
    out=$tmpdir/"${inf##*/}.cleanhosts"
    ## null routes
    grep ^0.0.0.0 "${inf}"| grep -v '^0\.0\.0\.0\ 0\.0\.0\.0$' | sed 's/0\.0\.0\.0 //'  | sed 's/#.*$//' | tr -d '\r' >> "$tmp"
    ## localhost routes
    grep ^127.0.0.1 "${inf}" | grep -v 'local' | sed 's/127\.0\.0\.1 //' | sed 's/#.*$//' | tr -d '\r' >> "$tmp"
    cat "$tmp" >> "$out"
    af=${inf##*/}
    actionfile=hosts_${af%%.*}.action
    printf "{+block{Hostfiles: %s}}\n" "${inf##*/}" > "${actionfile}"
    cat "$tmp" >> "${actionfile}"
    printf '%u entries in %s\n'  $(wc -l ${actionfile} | sed 's@/.*/@@')
    rm "$tmp"
done
echo "{+block{Hostfiles: consolidated}}" > ${action}
sort -u $tmpdir/*.cleanhosts >> ${action}
printf 'generated action file containing about %u unique entries.\n'  $(wc -l ${action} | cut -f 1 -d " ")
[ -d "$tmpdir" ] && rm -rf "$tmpdir"
