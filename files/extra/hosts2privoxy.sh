###
### simple script to convert blocking hosts files to privoxy filters
###
in="$1"
out="${1##*/}.action"
echo "{+block{${1##*/}}}" > ${out}
grep ^0.0.0.0 "${in}"| grep -v '^0\.0\.0\.0\ 0\.0\.0\.0$' | sed 's/0\.0\.0\.0 //' >> "$out"
