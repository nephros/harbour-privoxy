### AdBlock2Privoxy

... is a tool which can convert AdBlock lists to privoxy config files.
It also has a quite clever solution for the fact that Privoxy can not do element hiding well: it generates CSS files which implement the element hiding, and relies on a small http server running alongside privoxy to provide them.
Privoxy can then inject them into pages.

#### Using the provided example package

Because the generated files are quite large, they are distributed for
SailfishOS as a compressed package. To use them, extract the ab2p.tar.xz
package into the `extras/ab2p` folder. Don't be fooled, the 1MB package
decompresses into hundred(s) of megabytes!!

The provided example package is built from the configuration seen in
`ab2p_general.task`. Read it to find which adblock lists have been used.

#### Updating the provided example package
There is an automated process building updated versions of the tarball.
You can check out https://gitlab.com/nephros/harbour-privoxy/-/releases

This also includes two script that make updating easier.

#### Building with custom lists.

To get this working you must generate the custom blocking configuration on a PC where you have a haskell runtime available.
OR, you could use my Gitlab CI template at
[gitlab.com/nephros](https://gitlab.com/nephros/ci-templates/-/blob/master/other/ab2p-builder.yml)
to let docker do all the hard work.

On a local PC:
1. Get the source from https://github.com/FunCyRanger/adblock2privoxy
2. Compile the too according to the instructions for adblock2privoxy
3. Generate blocking .action, .filter and CSS files:
  - you MUST use the following options for the httpd provided with this package:
  - `stack run adblock2privoxy -- -p ./ab2p/ -w ./ab2p/css -d 127.0.0.1:8119 <<URLS for filter files>>`

WARNING: Large files will need more memory, more CPU, and make the proxy operate slower.  
Be careful which filter lists you choose to convert.

#### To enable the AB2P functionality on the device:

1. Copy the files onto the Sailfish device, into the `/conf/extras/ab2p` folder. It should end up looking like this:
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/`
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.action`
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.system.action`
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.filter`
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.system.filter`
  - `/usr/share/harbour-privoxy/conf/extra/ab2p/css/`
2. Add the appropriate action and filterfile options to the privoxy config file
  - `actionsfile extra/ab2p/ab2p.action`
  - `actionsfile extra/ab2p/ab2p.system.action`
  - `filterfile extra/ab2p/ab2p.filter`
  - `filterfile extra/ab2p/ab2p.system.filter`
1. make sure the harbout-privoxy-httpd.service is running. It should start together with the privoxy service automatically.

