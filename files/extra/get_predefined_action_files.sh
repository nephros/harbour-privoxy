#!/bin/sh
#
printf 'Downloading DandelionSprout AntiMalware action file\n'
curl -s -L https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwarePrivoxy.action \
	| sed 's/?/\\?/g' > AntiMalwarePrivoxy.action
#	| grep -v '&\*=\*' > AntiMalwarePrivoxy.action
printf 'Downloading DandelionSprout NordicFilters action file\n'
curl -s -L https://raw.githubusercontent.com/DandelionSprout/adfilt/master/NorwegianExperimentalList%20alternate%20versions/NordicFiltersPrivoxy.action \
		| sed 's/[^^]#\([^}]\)/%23\1/' \
		| sed 's/?/\\?/g' \
		| sed 's/[()]//g' > NordicFiltersPrivoxy.action
#		sed -e 's/\([a-z0-9]\)#.*\([}]\)$/\1\2/'> NordicFiltersPrivoxy.action
printf 'Downloading qwIvan adfilter action file\n'
curl -s -LO https://raw.githubusercontent.com/qwIvan/privoxy-adfilter/master/adfilter.action

printf 'Checking syntax...\n'
echo > check.config
for f in *.action; do
	if [ -e $f ]; then
	printf 'actionsfile %s\n' $f >> check.config
	fi
done
for f in *.filter; do
	if [ -e $f ]; then
	printf 'filterfile %s\n' $f >> check.config
	fi
done
harbour-privoxy --config-test --no-daemon check.config || \
	printf 'Please fix any syntax errors before deploying!\n'
rm check.config
