###
### simple script to convert blocking hosts files to privoxy filters
###

out="hosts_pgl_yoyo_org.action"
printf "{+block{Hostlist from pgl.yoyo.org}}\n" > ${out}
tmp=$(mktemp)
printf "Downloading list...\n"
curl -s -L -o "${tmp}" "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=junkbuster&showintro=1&mimetype=plaintext"
grep -v ^# "${tmp}" >> ${out}
printf "Created/updated action file at %s.\nRemember to add it to your Privoxy config file.\n" "${out}" 
