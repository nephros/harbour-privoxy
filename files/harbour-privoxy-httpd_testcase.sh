prog='curl  -D- --silent --retry 1 -o /dev/null -w "%{http_code}"'
server=localhost:8119
echo;echo '==========';echo expect OK: /ab2p.css
$prog http://$server/ab2p.css
echo;echo '==========';echo expect OK: /debug/ab2p.css
$prog http://$server/debug/ab2p.common.css
echo;echo '==========';echo expect OK: /ab2p_common.css
$prog http://$server/ab2p.common.css
echo;echo '==========';echo expect OK: www.example.org/ab2p.css
$prog http://$server/www.google.com/ab2p.css
echo;echo '==========';echo expect OK: other.example.org/ab2p.css
$prog http://$server/notthere.google.com/ab2p.css
echo;echo '==========';echo expect 404 www.example.com/ab2p.css
$prog http://$server/www.example.com/ab2p.css
echo;echo '==========';echo expect image OK: blocked_128.png
$prog http://$server/blocked_128.png
echo;echo '==========';echo expect image OK: blocked_128.jpg
$prog http://$server/blocked_128.jpg

