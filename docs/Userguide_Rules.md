# User Guide: Add additional action files

**Table of Contents**

  - [Privacy redirects](#privacy-redirects)
  - [AdBlock lists: the AdBlock2Privoxy (AB2P) Method](#adblock-lists-the-adblock2privoxy-ab2p-method)
    * [Enable the AB2P functionality on the device](#enable-the-ab2p-functionality-on-the-device)
  - [Tools and lists available on the web](#tools-and-lists-available-on-the-web)
  - [Converting hosts files to filter files](#converting-hosts-files-to-filter-files)
  - [The deprecated "extra-lists" package](#the-deprecated--extra-lists--package)

----
## Privacy redirects

One of the simpler (and recently popular) ways of preserving privacy is to
redirect requests of popular websites to instances of privoxy-respecting
front-ends.

Here's a simple recipe how to do that:

Suppose you want to redirect Meta's image things site to an alternative
frontend, `https://gram.example.social`

```
#############################################################################
# Aliases
#############################################################################
{{alias}}

insta-redirect = \
  +redirect{s@^(http|https)://[^/]*/(.*)@$1://gram.example.social/$2@} \
  -hide-referrer

#############################################################################
# End of Aliases

{insta-redirect}
instagram.com
www.instagram.com
help.instagram.com
about.instagram.com
```

Another example: the invidious Youtube redirector, using some theme settings:

```
#############################################################################
# Aliases
#############################################################################
{{alias}}

youtube-redirect-watch = +redirect{s@^(http|https)://[^/]*/watch\?([v=]*.*)@$1://redirect.invidious.io/watch\?$2\&thin_mode=true\&quality=medium\&dark_mode=true\&controls=1\&player_style=youtube@} -hide-referrer -block

# End of Aliases

{youtube-redirect-watch}
.youtube.com/watch
.youtube.*/watch
.youtube.*/s/watch
```

For SailfishOS developers, it may be useful to always redirect to the old Qt documentation:

```
#############################################################################
# Aliases
#############################################################################
{{alias}}
#############################################################################
#
qt-doc-redirect = \
  +redirect{s@/qt-6/@/archives/qt-5.6/@}

# End of Aliases

{qt-doc-redirect}
doc.qt.io/qt-6/*

```

**HINT**: If you have i2p or tor support enabled, you can also redirect to `.onion` or `.i2p` sites.

Sources for redirect target sites can be e.g. found on:

 - https://privacyredirect.com/index.html
 - https://codeberg.org/LibRedirect/instances - use their data.json file, or generate one yourself using their tool. Includes tor and i2p sites.
 - often the source repos for browser extensions have useful lists

## The AdBlock2Privoxy (AB2P) Method

AdBlock2Privoxy is a tool which can convert AdBlock lists to Privoxy config
files.  It also has a quite clever solution for the fact that Privoxy can not
do element hiding well: it generates CSS files which implement the element
hiding, and relies on a small http server running alongside Privoxy to
serve them.
Privoxy can then inject the hiding CSS snippets into pages.

You have three possibilities to get such converted files:

### Option 1: Using the provided example package

`harbour-privoxy` ships with an example package for AB2P.  You can install the
`harbour-privoxy-ab2p` RPM package. It is available in the same repository as
the harbour-privoxy package but will not show up in Storeman.  So use pkcon or
zypper. It will show up in Chum GUI though under additional packages.

Because the generated files are quite large, they are distributed for
SailfishOS as a compressed package. To use them, extract the ab2p.tar.xz
package into the `extras/ab2p` folder. Don't be fooled, the 1MB package
decompresses into hundred(s) of megabytes!!

The provided example package is built from the configuration seen in
`ab2p_general.task`. Read it to find which adblock lists have been used.

Note the example package is convenient, but not updated very often. It is best
to update its files from one of the other methods after becoming familiar with
AB2P.

### Option 2: Using automatically generated tarballs

This GitLab project uses CI to build a set of preconfigured tarballs every four
weeks. Hop over to the
[Releases](https://gitlab.com/nephros/harbour-privoxy/-/releases) section to
get them.

They come in three variants:

 - **general**: the same configuration that comes with the example package, but likely more up-to-date.
 - **noelemhide**: generated from the EasyList "No Element Hiding" list, offers blocking only with none of the CSS tricks.
 - **nephros**: Yours Truly's current/preferred configuration. May fit you needs as well, or may not.

On the download page you will also find a script you can run (after you have
installed the files) to update from the web if desired.

### Option 3: Building your own variant with custom lists

To get this working you must generate the custom blocking configuration on a PC where you have a haskell runtime available.
OR, you could use my Gitlab CI template at
[gitlab.com/nephros](https://gitlab.com/nephros/ci-templates/-/blob/master/other/ab2p-builder.yml)
to let docker do all the hard work.

On a local PC:
1. Get the source from https://github.com/FunCyRanger/adblock2privoxy
2. Compile the tool according to the instructions for adblock2privoxy
3. Generate blocking .action, .filter and CSS files:
  - you MUST use the following options for the httpd provided with this package:
  - `stack run adblock2privoxy -- -p ./ab2p/ -w ./ab2p/css -d 127.0.0.1:8119 <<URLS for filter files>>`

WARNING: Converting large files, or large amounts of files, will lead to
Privoxy needing more memory, more CPU, and make the proxy operate slower.
Be careful and reasonable about which filter lists you choose to convert.

### Enable the AB2P functionality on the device

Once you have your converted files, make Privoxy aware of them:

1. Copy the files onto the Sailfish device (or extract them there), into the `conf/extras/ab2p` folder. It should end up looking like this:

```
    /usr/share/harbour-privoxy/conf/extra/ab2p/
    /usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.action
    /usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.system.action
    /usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.filter
    /usr/share/harbour-privoxy/conf/extra/ab2p/ab2p.system.filter
    /usr/share/harbour-privoxy/conf/extra/ab2p/css/
    /usr/share/harbour-privoxy/conf/extra/ab2p/css/... # <-- lots of files and dirs below here
```

2. Add the appropriate action and filterfile options to the privoxy config file

```
     actionsfile extra/ab2p/ab2p.action
     actionsfile extra/ab2p/ab2p.system.action
     filterfile extra/ab2p/ab2p.filter
     filterfile extra/ab2p/ab2p.system.filter
```

3. make sure the `harbour-privoxy-httpd.service` is running. It should start
   alongside the privoxy service automatically.


## Tools and lists available on the web

There are some ways to generate additional filter/action files from online
sources such as AdBlock Plus.

There are several projects to check out:

  - https://filterlists.com/ <-- You can filter for Privoxy action files
  - https://www.fabiankeil.de/sourcecode/pft/
  - https://github.com/essandess/adblock2privoxy <-- this is the base for the AD2P method described earlier
  - https://github.com/Andrwe/privoxy-blocklist
  - https://github.com/FunCyRanger/privoxy-blocklist/

## Premade Privoxy Action files

  A website which generates ready-made files. Select the "junkbuster" format for that one!

  - https://pgl.yoyo.org/adservers/

A script called `get_yoyo_org.sh` is provided in the `extras` directory which does this.

## Regex lists

  A reasonable alternative to either ab2p and hosts list is to use regexes.
  While not as sophisticated as ab2p (no element hiding), they are still more
  optimized for Privoxy than converting a hosts list

These are sites hosting block lists in regex format:

  - https://oisd.nl/setup

A script called `get_oisd_nl.sh` is provided in the `extras` directory which does this.

## Converting hosts files to filter files

This is quite trivial. If you have a hosts file you can convert it to a simple
privoxy actions file.  Find scripts `hosts2privoxy.sh` and
`hosts2privoxy_dl.sh` in the `extras` directory which do this.

These have only been tested with some hosts files, so might require slight
modifications for others, but you can see the format.

If you are also using [Defender II](https://openrepos.net/content/peterleinchen/defender-ii-updated-encrypted-devices-originated-nodevel)
you can re-use its list if you want.

Doing this is effective, but offers quite unsophisticated protection. Also,
these lists tend to be large, leading to memory and performance problems.

----
## The deprecated "extra-lists" package

**DEPRECATION NOTICE**: The plain conversion scripts from this package never
worked very well, so there will be no updates to these.  This section here is
for older releases which have the `-extra-lists` package.

It is recommended to use the AB2P method.

----
Experimental pre-generated files, and a slightly modified version of the script
from the `privoxy-blocklist` project are available in the
`harbour-privoxy-extra-lists` package. You will have to use zypper or pkcon to
install it.

To use the pre-generated files:

 - decompress the .xz files you want to use
 - activate/add the corresponding `actionsfile` and `filterfile` directives in `config.sailfish`

See [EasyList.to](https://easylist.to) for information what the lists do exactly.

Not all of them are tested, but Yours Truly uses easyprivacy, fanboy-social,
and antiadblock successfully.

**NOTE:** the conversion script is not perfect. It can generate some rules that:
 - may cause privoxy to write errors about filters to the log. These are harmless
 - may cause privoxy startup to fail. This is worse of course. To correct:
   - enable logging in the config file
   - run harbour-privoxy --no-daemon --config-test /path/to/config
   - check for Fatal error lines in the log (grep Fatal logfile)
   - the error will give the line number and filename for the offending rule. Edit the file and correct/remove it
   - you may have to repeat this a couple of times to catch all of them...

You may call the `privoxy-blocklist_check_and_fix.sh` script to check the files beforehand.

