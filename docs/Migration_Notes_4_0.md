## Privoxy for Sailfish OS: Migration from 3.0.XX to 4.0+

With the release of Privoxy 4.0, the packaging for Sailfish OS underwent some
changes that require user interaction.  
This guide collects them.

### From mbedTLS to wolfSSL

Privoxy 4.0 supports a third TLS library for its HTTPS Inspection feature: wolfSSL.
Previous versions supported Open/LibreSSL and mbedTLS.

The Sailfish OS version of it chose to use mbedTLS up until now.

With 4.0:

 - we switched to wolfSSL, because of *impressive* improvements in performace (i.e. browsing speed).
 - in addition, we implemented a patch against upstream which uses Elliptic
   Curve Cryptography instead of RSA for the internally-created certificates.
   According to our tests, this further improves performance.

#### What's to do?

  1. Remove the mbedTLS package after the update to Privoxy 4.0
  2. Clean the TLS certificates from the cache dir

When installing Privoxy 4.0, the required wolfssl library will be installed automatically.
However, if you were previously using Privoxy 3.0.xx, mbedTLS will remain on the system.

You can remove it like so. Doing this is *optional* but recommended:

    devel-su pkcon remove mbedtls-libs

Obviously, don't do this if you have other software which used the library. At
the time of this writing, we no of no other users of it though.

Also Privoxy will have generated RSA-based certificates, which can't be used by
the new version. It is recommeded to clean them from the cache location:


    devel-su rm /usr/share/harbour-privoxy/ssl/certs/*.pem
    devel-su rm /usr/share/harbour-privoxy/ssl/certs/*.crt

*NOTE*: The [CA certificate](docs/Userguide_HTTPS.md#generate-a-ca-certificate)
will continue to use an RSA key, and can remain as-is.  
We may switch to an ECC-based CA certificate at a later date.

