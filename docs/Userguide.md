# User Guide

**Table of Contents**

- [User Guide](#user-guide)
  * [Enable the service](#enable-the-service)
  * [Configure Sailfish OS to use the Proxy](#configure-sailfish-os-to-use-the-proxy)
    + [Add additional action files](#add-additional-rules-and-action-files)
  * [HTTPS support](#https-support)
  * [Other stuff](#other-stuff)
    + [Enable the local documentation](#enable-the-local-documentation)
    + [Tor and I2P integration](#tor-and-i2p-integration)
  * [Housekeeping and Plumbing](#housekeeping-and-plumbing)

----

## Enable the service
Start the systemd service (as root)

    # systemctl enable harbour-privoxy.service
    # systemctl start  harbour-privoxy.service

## Configure Sailfish OS to use the Proxy

Configure your application to use a proxy at http://127.0.0.1:8118.  

You can do that through `Settings -> Mobile Network -> Advanced`.  (Note that
many applications do not actually use the "Global Proxy" option.

One notable example of software that DOES respect the Global Proxy setting is
the SailfishOS package update and install mechanism, so the Store App, System
Updates, `pkcon`, `zypper` and so on go through the proxy to download packages.
So, be very careful with your filter and blocking configurations, or you might
actually break these temporarily.

For the Sailfish Browser, proxy settings can be done on the about:config page:

    network.proxy.http = 127.0.0.1
    network.proxy.http_port = 8118
    network.proxy.type = 1

and maybe also

    network.http.proxy.pipelining = false

If you find an app that does not respect the "Global Proxy" setting, e.g. apps
using QtWebView components, you can coax it into using it by setting the
environment variable `http_proxy`. Again, depending on the app it may or may
not actually respect that. But Qt components at least do.

    env http_proxy=http://127.0.0.1:8118 harbour-appname

And you're good to go. Test your configuration by browsing to [http://p.p](http://p.p)

Please refer to the [Privoxy documentation](https://www.privoxy.org/user-manual/quickstart.html) on where to go from here.

**Notes about the SailfishOS package**

Some things are changed from the upstream distribution:

 - The daemon is run from systemd in system context, but as defaultuser/nemo
 - most things have been renamed from `privoxy` to `harbour-privoxy`
 - configuration lives under `/usr/share/harbour-privoxy/conf`, not `/etc/privoxy`
 - in order to run the daemon and access the config files, the user must be in the `inet` group

### Add additional rules and action files

See the [Rules page](Userguide_Rules.md) for instructions.

## HTTPS support

As most of the WWW is HTTPS nowadays, having Privoxy only act on HTTP pages is
not very useful.  To enable the support for HTTPS inspection, additional steps
are necessary.

See the  [HTTPS support page](Userguide_HTTPS.md) for instructions.


## Other stuff
### Enable the local documentation

If you want the documentation, you can install the `harbour-privoxy-docs` RPM
package. It is available in the same repository as the harbour-privoxy package
but will not show up in Storeman.
So use pkcon or zypper.

You will then need the config file to say (this should be enabled by default):

    user-manual  /usr/share/doc/harbour-privoxy/user-manual

Having the docs available locally is useful as the config web page links to it
in places.

### Tor and I2P integration

You can use this in combination with the Tor proxy. Just add/uncomment the line

    forward-socks5t / 127.0.0.1:9050 .

in Section 5.2 of `config.sailfish` (it's around line 1400).

For I2P, running a local i2pd, use:

    forward-socks5    /  127.0.0.1:4447 .

To only use the tor/i2p networks for hidden services, make this:

    forward-socks5t    *.onion  127.0.0.1:4445 .
    forward-socks5     *.i2p  127.0.0.1:4447 .

----

## Housekeeping and Plumbing

harbour-privoxy ships with these SystemD units:

  - `harbour-privoxy.service`				<-- the main service
  - `harbour-privoxy-httpd.service`			<-- minimal http server for AB2P, serves local content (css, fonts etc)
  - `harbour-privoxy-log2jrnl.service`			<-- forwards entries from the log file to journald

  - `harbour-privoxy-housekeeping.timer`		<-- this triggers the housekeeping services every 24 hours
  - `harbour-privoxy-housekeeping.target`		<-- target which starts the others, below
  - `harbour-privoxy-clean-certs.service`		<-- member of housekeeping, cleans out the generated https certs
  - `harbour-privoxy-clean-log.service`			<-- member of housekeeping, empties the log file from time to time

