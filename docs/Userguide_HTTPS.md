# User Guide: HTTPS support

**Table of Contents**

  - [Generate a CA certificate](#generate-a-ca-certificate)
  - [Add the CA certificate to the Sailfish Browser trust list (NSS)](#add-the-ca-certificate-to-the-sailfish-browser-trust-list--nss-)
  - [Add the CA certificate to the local Sailfish trust list (OpenSSL)](#add-the-ca-certificate-to-the-local-sailfish-trust-list--openssl-)
  - [Make the Browser use a proxy for HTTPS as well](#make-the-browser-use-a-proxy-for-https-as-well)

----

To enable the support for HTTPS inspection, additional steps are necessary.

**NOTE:** This basically works by doing SSL/TLS MITM (man-in-the-middle) using
widely accessible, system-wide trusted certificate.  
This is a *tremendous* security risk and opens up all your internet usage to
potential tampering.

Be careful about what you're doing here.

## Generate a CA certificate

Generate Certificate CA files necessary for applications to trust Privoxy:

1. Go to `/usr/share/harbour-privoxy/ssl/ca`
1. Inspect the `generate-ca-certs.sh` script and `harbour-privoxy-ca.cnf` OpenSSL config file to make sure they do what you want. Note that if you change the password, you will have to change the `ca-password`  directive in `conf.sailfish` as well.  
1. Run `/bin/sh generate-ca-certs.sh`
1. make sure the file names and locations match the ones configured in section 7 of `conf.sailfish`
1. check permissions on the files and directory, you don't want anyone to steal and replace these

Now that you have the certificates, note that *uninstalling* harbour-privoxy
will remove them. Pure updates should leave them intact.

As a final step, we need to give Privoxy a list of trusted CAs. This is the file name given in the `trusted-cas-file` directive, and its default is `trustedCAs.pem`.
Luckily we do not need to build that, the OS already has a suitable file. Let's just symlink it:

    # ln -s /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem /usr/share/harbour-privoxy/ssl/ca/trustedCAs.pem

## Add the CA certificate to the Sailfish Browser trust list (NSS)

You need to add the CA as trusted to the Mozilla certificate store, otherwise
the browser will not accept any https connections.  You may have to install the
`nss-tools` package to get `certutil`.  
See [this post](https://together.jolla.com/question/835/browser-personal-certificates-import/?answer=8170) for more.


If you want to install it for **all users**, run as root:

    # certutil -A -n "Privoxy CA" -t "TC,," -d /etc/pki/nssdb -i /usr/share/harbour-privoxy/ssl/ca/privoxy-ca-cert.crt

**OR** for one user only, as user, **under SailfishOS versions 4.0 and above**:

    $ certutil -A -n "Privoxy CA" -t "TC,," -d ${HOME}/.local/share/org.sailfishos/browser/.mozilla/ -i /usr/share/harbour-privoxy/ssl/ca/privoxy-ca-cert.crt

**OR** for one user only, as user, **under SailfishOS versions < 4.x**:

    $ certutil -A -n "Privoxy CA" -t "TC,," -d ${HOME}/.mozilla/mozembed/ -i /usr/share/harbour-privoxy/ssl/ca/privoxy-ca-cert.crt

To check that is has been installed:

    certutil -L -d ${HOME}/.local/share/org.sailfishos/browser/.mozilla


Other applications which use Silica WebView may need this also. In that case,
the .mozilla location should be under
`${HOME}/.cache/<<OrganizationName>>/<<ApplicationName>>/.mozilla`.

This will make the Sailfish Browser, and other mozilla-based applications, trust the new CA certificate.
To check that it is working, open any https:// website and tap the little padlock in the address bar.
It should show your certificate as CA.

Removing the certificate again:

    $ certutil -D -n "Privoxy CA" -d ${HOME}/.local/share/org.sailfishos/browser/.mozilla/

**OR**

    # certutil -D -n "Privoxy CA" -t "TC,," -d /etc/pki/nssdb -i /usr/share/harbour-privoxy/ssl/ca/privoxy-ca-cert.crt

## Add the CA certificate to the local Sailfish trust list (OpenSSL)

SSL-enabled application apart from the browser may use the OpenSSL certificate
store instead of NSS. So your CA needs to be there and trusted as well.

This should be done according to the procedure laid out in `/etc/pki/ca-trust/source/README`.  
So, take the certificate just generated, and place it there:

    # cp /usr/share/harbour-privoxy/ssl/ca/privoxy-ca-cert.crt /etc/pki/ca-trust/source/anchors/
    # update-ca-trust

You can check whether it worked by going to Settings -> Certificates -> TLS and
search for "Harbour"

## Make the Browser use a proxy for HTTPS as well

Like above, configure the Browser to use the proxy:

    network.proxy.ssl = 127.0.0.1
    network.proxy.ssl_port = 8118

Now, there are things like HSTS, csp, and others which are supposed to make you
safer, but contribute to sites breaking due to ad blocking.  Some settings will
improve behaviour, but reduce security/safety. Some of them are given below.  
You may want to study [arkenfox](https://github.com/arkenfox/user.js) for
detailed info.

*Caveat Emptor*.

    security.mixed_content.block_active_content
    browser.xul.error_pages.expert_bad_cert
    security.csp.enable
    security.ssl.enable_ocsp_must_staple
    security.ssl.enable_ocsp_stapling


