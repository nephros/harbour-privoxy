# harbour-privoxy: Privoxy for SailfishOS

Privoxy is a non-caching web proxy with advanced filtering capabilities for
enhancing privacy, modifying web page data and HTTP headers, controlling
access, and removing ads and other obnoxious Internet junk. Privoxy has a
flexible configuration and can be customized to suit individual needs and
tastes. It has application for both stand-alone systems and multi-user
networks.

 - original software: https://www.privoxy.org/
 - git repo for this build: https://gitlab.com/nephros/harbour-privoxy



----
## IMPORTANT NOTICE about 4.0

Some things have changed. Please read the [migration guide](docs/Migration_Notes_4_0.md).

----
## About Sailfish OS Upgrades

If you came here because of a scary warning in the Release Notes, they are
correct that Privoxy can affect the update process. **But only** if you:

 - use HTTPS inspection
 - have privoxy configured as a **Global Proxy** and have this setting enabled
 - OR have a systemd-wide variable or other configuration which forces connections to go through privoxy always
 - and/or have heavily modified the blocking rules so access to Sailfish OS repositories is affected
 - and/or are routing all traffic over Tor ot I2P.

But in general, if you have had no problems updating software from the official
repositories with your Privoxy setup previously, it won't affect them now.

Still, just to be safe, disable Privoxy before trying to upgrade.  
See [https inspection](docs/Userguide_HTTPS.md) section in the [User Guide](docs/Userguide.md) for some details.

----

## Installation Instructions

See the User Guide on how to proceed after installation:

[User Guide](docs/Userguide.md)
